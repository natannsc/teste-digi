# Teste Digi

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org)

[![](https://img.shields.io/badge/-React.js-gray?logo=react)]()
[![](https://img.shields.io/badge/-Node.js-gray?logo=node.js)]()
[![](https://img.shields.io/badge/-Typescript-gray?logo=typescript)]()
[![](https://img.shields.io/badge/-MongoDB-gray?logo=mongodb)]()

[![](https://img.shields.io/badge/-GraphQL-gray?logo=graphql)]()
[![](https://img.shields.io/badge/-ESLint-gray?logo=eslint)]()
[![](https://img.shields.io/badge/-Prettier-gray?logo=prettier)]()

## Comandos

---

### Instalar dependências

```bash
npm install
```

### Rodar localmente (frontend e backend)

```bash
npm start
```

### Testes

```bash
// TODO
```

### Build

```bash
npm run build
```

## Candidato

---

Linkedin: https://www.linkedin.com/in/natan-dos-santos-camargos-7b8153122/

Github: https://github.com/thr0wn

Stack Overflow: https://stackoverflow.com/users/6489712/natan-camargos
