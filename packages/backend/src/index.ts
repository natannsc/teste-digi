import { ApolloServer, gql } from 'apollo-server';
import * as mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { UserModel } from './models';

const mongod = new MongoMemoryServer();

mongod
  .getUri()
  .then(uri => mongoose.connect(uri, { useNewUrlParser: true }))
  .then(() => console.info('Mongoose connected!'))
  .catch(error => console.error(error));

// The GraphQL schema
const typeDefs = gql`
  type User {
    id: ID
    name: String!
    cep: String!
    street: String!
    number: String!
    district: String!
  }

  type Query {
    users: [User]!
  }

  type Mutation {
    saveUser(
      name: String!
      cep: String!
      street: String!
      number: String!
      district: String!
    ): User
  }
`;

const resolvers = {
  Query: {
    users: () => UserModel.find({}),
  },
  Mutation: {
    saveUser: (context, params) => new UserModel(params).save(),
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.listen().then(({ url }) => {
  console.log(`Apollo server ready at ${url}`);
});
