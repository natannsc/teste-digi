import { Document, Schema, model, Model } from 'mongoose';

export const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  cep: {
    type: String,
    required: true,
  },
  street: {
    type: String,
    required: true,
  },
  number: {
    type: String,
    required: true,
  },
  district: {
    type: String,
    required: true,
  },
});

export interface IUserSchema extends Document {
  name: string;
  cep: string;
  street: string;
  number: string;
  district: string;
}

export interface IUserModel extends Model<IUserSchema> {}

UserSchema.pre<IUserSchema>('save', function() {
  this.cep = String(this.cep).replace(/[^\d]/g, '');
});

export const UserModel = model<IUserSchema, IUserModel>('User', UserSchema);
