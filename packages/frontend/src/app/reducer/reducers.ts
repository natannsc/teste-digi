import { Actions, Types } from './actions';
import { IUser } from './models';

export type AppState = {
  users: IUser[];
  loading: boolean;
  error: boolean;
};

export const appInitialState: AppState = {
  users: [],
  loading: false,
  error: false
};

export const appReducer = (state = appInitialState, action: Actions): AppState => {
  switch (action.type) {
    case Types.SAVE_USER:
    case Types.FETCH_USERS:
      return { ...state, loading: true };
    case Types.SAVE_USER_SUCCESS:
      return { ...state, loading: false, error: false };
    case Types.FETCH_USERS_SUCCESS:
      return { ...state, loading: false, error: false, users: action.users };
    case Types.SAVE_USER_ERROR:
    case Types.FETCH_USERS_ERROR:
      return { ...state, loading: false, error: true };
  }
  return state;
};
