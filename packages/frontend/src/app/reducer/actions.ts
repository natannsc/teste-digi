import { IUser } from './models';
import { Dispatch } from 'react';
import { ApolloClient, gql } from 'apollo-boost';

export enum Types {
  FETCH_USERS = 'FETCH_USERS',
  FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS',
  FETCH_USERS_ERROR = 'FETCH_USERS_ERROR',
  SAVE_USER = 'SAVE_USER',
  SAVE_USER_SUCCESS = 'SAVE_USER_SUCCESS',
  SAVE_USER_ERROR = 'SAVE_USER_ERROR'
}

interface FetchUsersAction {
  type: Types.FETCH_USERS;
}

interface FetchUsersSuccessAction {
  type: Types.FETCH_USERS_SUCCESS;
  users: IUser[];
}

interface FetchUsersErrorAction {
  type: Types.FETCH_USERS_ERROR;
}

interface SaveUserAction {
  type: Types.SAVE_USER;
}

interface SaveUserSuccessAction {
  type: Types.SAVE_USER_SUCCESS;
  user: IUser;
}

interface SaveUserErrorAction {
  type: Types.SAVE_USER_ERROR;
}

export const getUsers = async (client: ApolloClient<unknown>, dispatch: Dispatch<Actions>) => {
  dispatch({ type: Types.FETCH_USERS });

  try {
    const response = await client.query({
      fetchPolicy: 'no-cache',
      query: gql`
        {
          users {
            name
            cep
            street
            number
            district
          }
        }
      `
    });
    dispatch({ type: Types.FETCH_USERS_SUCCESS, users: response.data.users });
  } catch (error) {
    dispatch({ type: Types.FETCH_USERS_ERROR });
  }
};

export const saveUser = async (user: IUser, client: ApolloClient<unknown>, dispatch: Dispatch<Actions>) => {
  dispatch({ type: Types.SAVE_USER });

  try {
    const response = await client.mutate({
      variables: user,
      mutation: gql`
        mutation saveUser($name: String!, $cep: String!, $street: String!, $number: String!, $district: String!) {
          saveUser(name: $name, cep: $cep, street: $street, number: $number, district: $district) {
            name
            cep
            street
            number
            district
          }
        }
      `
    });
    dispatch({ type: Types.SAVE_USER_SUCCESS, user: response.data });
  } catch (error) {
    dispatch({ type: Types.SAVE_USER_ERROR });
    throw error;
  }
};

export type Actions =
  | SaveUserAction
  | SaveUserSuccessAction
  | SaveUserErrorAction
  | FetchUsersAction
  | FetchUsersSuccessAction
  | FetchUsersErrorAction;
