export interface IUser {
  name: string;
  cep: string;
  street: string;
  number: number;
  district: string;
}
