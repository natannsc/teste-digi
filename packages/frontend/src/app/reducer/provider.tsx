import React, { useReducer, Dispatch, useEffect } from 'react';
import { appReducer, appInitialState, AppState } from '.';
import { Actions, getUsers } from './actions';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
  uri: process.env.SERVER_URL
});

export const AppContext = React.createContext<{
  state: AppState;
  dispatch: Dispatch<Actions>;
}>({
  state: appInitialState,
  dispatch: () => null
});

export const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, appInitialState);

  useEffect(() => {
    getUsers(client, dispatch);
  }, []);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <ApolloProvider client={client}>{children}</ApolloProvider>
    </AppContext.Provider>
  );
};
