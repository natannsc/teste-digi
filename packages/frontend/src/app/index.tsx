import React from 'react';
import { hot } from 'react-hot-loader';
import UserForm from './components/UserForm';
import { AppProvider } from './reducer';
import UserList from './components/UserList';
import { Row, Col } from 'antd';

const App = () => (
  <AppProvider>
    <Row>
      <Col>
        <UserForm />
      </Col>
      <Col>
        <UserList />
      </Col>
    </Row>
  </AppProvider>
);

export default hot(module)(App);
