import React from 'react';
import { Field as ReactFinalFormField } from 'react-final-form';
import { Form as AntForm, Input } from 'antd';
import { FormItemProps } from 'antd/lib/form';
import { required } from '../validators';

const defaultLayout = {
  labelCol: { span: 6 }
};

interface IFieldProps {
  name: string;
  label: string;
  parse?: (value: string) => string;
  layout?: Partial<FormItemProps>;
}

const Field: React.FC<IFieldProps> = ({ name, label, layout = defaultLayout, parse }) => (
  <ReactFinalFormField name={name} validate={required}>
    {({ input, meta }) => (
      <AntForm.Item
        {...layout}
        name={name}
        label={label}
        validateStatus={meta.touched && meta.error ? 'error' : 'success'}
        help={(meta.touched && meta.error) || undefined}
        normalize={parse}
      >
        <Input {...input} />
      </AntForm.Item>
    )}
  </ReactFinalFormField>
);

export default Field;
