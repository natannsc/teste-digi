enum Messages {
  REQUIRED = 'Campo obrigatório'
}

export const required = (value: string) => (value ? '' : Messages.REQUIRED);
