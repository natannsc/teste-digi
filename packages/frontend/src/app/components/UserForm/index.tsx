import React, { useContext } from 'react';
import { Card, Row, Col, Button, Spin, message } from 'antd';
import { Form } from 'react-final-form';
import { Gutter } from 'antd/lib/grid/row';
import { Form as AntForm } from 'antd';
import formatStringByPattern from 'format-string-by-pattern';
import { useApolloClient } from '@apollo/react-hooks';
import Field from './Field';
import { AppContext, saveUser, IUser, getUsers } from 'app/reducer';
import './index.css';

const fullWithFieldLayout = { labelCol: { span: 3 } };
const gutter: [Gutter, Gutter] = [8, 12];
const formatOnlyNumbersByPattern = (pattern: string) => (value = '') =>
  formatStringByPattern(pattern)(value.replace(/[^\d]/g, ''));

const UserForm = () => {
  const { state, dispatch } = useContext(AppContext);
  const client = useApolloClient();

  const onSubmit = async (values: IUser) => {
    try {
      await saveUser(values, client, dispatch);
      await getUsers(client, dispatch);
      message.success('Usuário cadastrado com sucesso');
    } catch (error) {
      message.error('Desculpe, mas não foi possível cadastrar o usuário. Tente novamente mais tarde.');
    }
  };

  return (
    <div className="user-form">
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, submitting }) => (
          <Spin spinning={state.loading}>
            <Card
              title="Cadastro de usuário"
              actions={[
                <Button type="primary" htmlType="button" onClick={handleSubmit} disabled={submitting}>
                  Cadastrar
                </Button>
              ]}
            >
              <AntForm onFinish={handleSubmit}>
                <Row gutter={gutter}>
                  <Col span={24}>
                    <Field name="name" label="Nome" layout={fullWithFieldLayout} />
                  </Col>
                </Row>
                <Row gutter={gutter}>
                  <Col span={12}>
                    <Field name="cep" label="CEP" parse={formatOnlyNumbersByPattern('99999-999')} />
                  </Col>
                  <Col span={12}>
                    <Field name="number" label="Número" parse={formatOnlyNumbersByPattern('999999999')} />
                  </Col>
                </Row>
                <Row gutter={gutter}>
                  <Col span={24}>
                    <Field name="street" label="Rua" layout={fullWithFieldLayout} />
                  </Col>
                </Row>
                <Row gutter={gutter}>
                  <Col span={24}>
                    <Field name="district" label="Bairro" layout={fullWithFieldLayout} />
                  </Col>
                </Row>
              </AntForm>
            </Card>
          </Spin>
        )}
      />
    </div>
  );
};

export default UserForm;
