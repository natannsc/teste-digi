import React, { useContext } from 'react';
import { Card, Row, Col, Avatar, Spin } from 'antd';
import { AppContext } from 'app/reducer';
import './index.css';
import Meta from 'antd/lib/card/Meta';

const UserList = () => {
  const { state } = useContext(AppContext);

  return (
    <Card title="Usuários cadastrados" className="user-list">
      <Spin spinning={state.loading}>
        <Row>
          {state.users.map((user, index) => (
            <Col span={12}>
              <Card>
                <Meta
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  title={user.name}
                  description={`CEP: ${user.cep}`}
                />
              </Card>
            </Col>
          ))}
        </Row>
      </Spin>
    </Card>
  );
};

export default UserList;
